import React from 'react';
import './App.css';
import Input from './components/Input';
import TodoItem from './components/TodoItem';

import { useSelector } from 'react-redux'
import { selectTodoList } from './features/todoSlice'


function App() {

  const todoList = useSelector(selectTodoList)

  return (
    <div className="App">
        <div className="app__container">
        <h1>TO DO LIST</h1>
          <div className="app__todoContainers">
            {
              todoList.map((item, index)=> (
                <TodoItem  
                  id = {item.index}
                  title= {item.title}
                  description= {item.description}
                  status= {item.status}
                  createdAt= {item.createdAt}
                  />
              ))
            }
          </div>

          <Input/>
        </div> 
    </div>
  );
}

export default App;
