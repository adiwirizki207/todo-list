import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    todoList: []
}


// console.log(dataApi)

const todoSlice = createSlice({
    name: 'todos',
    initialState,
    reducers: {
        saveTodo: (state, action) => {
            state.todoList.push(action.payload)
        },
        setCheck: (state, action) => {
            state.todoList.map(item=>{
                if (action.payload === item.createdAt){
                    if(item.status === true){
                        item.status = false
                    }else{
                        item.status = true
                    }
                }
            })
        }
    }
});



export const { saveTodo, setCheck } = todoSlice.actions

export const selectTodoList = state => state.todos.todoList


export default todoSlice.reducer