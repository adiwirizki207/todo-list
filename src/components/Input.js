import React, { useState } from 'react'
import'./Input.css'

import {useDispatch}  from 'react-redux'
import {saveTodo} from '../features/todoSlice'

const Input = () => {
    
    const [input, setInput ] = useState('')
    const [descInput, setDesc ] = useState('')
    const dispatch = useDispatch()

    const addTodo = () =>{

        dispatch(saveTodo({
            id: Number,
            title: input,
            description: descInput,
            status: false,
            createdAt: Date.now(),
        }))
    }
    
    return (
        <div className = 'input'>

            <input type = 'text' value = {input} onChange = {e=>setInput(e.target.value)} placeholder="Title"/>
            <textarea value = {descInput} onChange = {e=>setDesc(e.target.value)} placeholder="Description"/>
            <button onClick = {addTodo}> Tambah </button>
        </div>
    )
}

export default Input
