import React from 'react'
import './TodoItem.css'

import Checkbox from '@material-ui/core/Checkbox';

import { useDispatch } from 'react-redux';
import { setCheck } from '../features/todoSlice';

const TodoItem = ({title, status, createdAt, description}) => {
    const dispatch = useDispatch()


    const handleCheck = () => {
        dispatch(setCheck(createdAt))    
    }
    
    const updateTodo = () => {
        // dispatch(todoUpdate());
    }
    
    const deleteTodo = () => {
        // dispatch(todoDelete());
        
    }

    return (
        <div className='todoList'>
            <div className= 'todoItem'>
                <Checkbox
                    checked={status}
                    color="primary"
                    onChange={handleCheck}
                    inputProps={{ 'aria-label': 'primary checkbox' }}
                    />
                
                <p className={status && 'todoItem--done'}>{title}</p>
                <button onClick= {updateTodo}>Update</button>
                <button onClick= {deleteTodo}>Delete</button>
            </div>
            <div className = "descContainer">
                <p className="descList">{description}</p>
            </div>
        </div>
    )
}

export default TodoItem 
